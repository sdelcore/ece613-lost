declare -a images=(
    "000021" "000041" "000005" # SOME IMAGE NAME
)

DATASET_PATH=/root/lost/datasets/VOC2007/VOCdevkit/VOC2007/JPEGImages
LOST_PATH=~/lost
DINO_PATH=$LOST_PATH/dino
OUTPUT_PATH=$LOST_PATH/outputs/samples

DINO_ARCH=vit_base
LOST_FEATURES=k
K_PATCHES=10
PATCH_SIZE=16
NUM_INIT_SEEDS=100

rm -rf $OUTPUT_PATH

for i in "${images[@]}"
do
    echo evaluating $i
    mkdir -p $OUTPUT_PATH/$i
    cd $LOST_PATH
    
    python main_lost.py \
        --image_path $DATASET_PATH/$i.jpg \
        --output_dir $OUTPUT_PATH/$i \
        --arch $DINO_ARCH \
        --which_feature $LOST_FEATURES \
        --k_patches $K_PATCHES \
        --visualize pred \
        --num_init_seeds $NUM_INIT_SEEDS

    python main_lost.py \
        --image_path $DATASET_PATH/$i.jpg \
        --output_dir $OUTPUT_PATH/$i \
        --arch $DINO_ARCH \
        --which_feature $LOST_FEATURES \
        --k_patches $K_PATCHES \
        --visualize fms \
        --num_init_seeds $NUM_INIT_SEEDS

    python main_lost.py \
        --image_path $DATASET_PATH/$i.jpg \
        --output_dir $OUTPUT_PATH/$i \
        --arch $DINO_ARCH \
        --which_feature $LOST_FEATURES \
        --k_patches $K_PATCHES \
        --visualize seed_expansion \
        --num_init_seeds $NUM_INIT_SEEDS

done