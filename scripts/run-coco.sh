
OUTPUT_PATH=/root/lost/datasets/COCO/outputs/coco

DINO_ARCH=vit_base
LOST_FEATURES=k
K_PATCHES=100
PATCH_SIZE=16
NUM_INIT_SEEDS=100

cd /root/lost/
rm -rf $OUTPUT_PATH
mkdir -p $OUTPUT_PATH
echo $OUTPUT_PATH

python main_lost.py \
    --dataset COCO20k \
    --set train \
    --output_dir $OUTPUT_PATH \
    --arch $DINO_ARCH \
    --which_feature $LOST_FEATURES \
    --k_patches $K_PATCHES \
    --patch_size $PATCH_SIZE \
    --visualize pred \
    --num_init_seeds $NUM_INIT_SEEDS

exit