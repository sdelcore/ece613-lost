
OUTPUT_PATH=/root/lost/datasets/COCO/outputs/coco



cd /root/lost/
rm -rf $OUTPUT_PATH
mkdir -p $OUTPUT_PATH
echo $OUTPUT_PATH

echo "COCO vit_base k=100 p=16, num_init_seed=1"
DINO_ARCH=vit_base
LOST_FEATURES=k
K_PATCHES=100
PATCH_SIZE=16
NUM_INIT_SEEDS=1

python main_lost.py \
    --dataset COCO20k \
    --set train \
    --output_dir $OUTPUT_PATH \
    --arch $DINO_ARCH \
    --which_feature $LOST_FEATURES \
    --k_patches $K_PATCHES \
    --patch_size $PATCH_SIZE \
    --num_init_seeds $NUM_INIT_SEEDS

echo "COCO vit_base k=100 p=16, num_init_seed=100"
DINO_ARCH=vit_base
LOST_FEATURES=k
K_PATCHES=100
PATCH_SIZE=16
NUM_INIT_SEEDS=100

python main_lost.py \
    --dataset COCO20k \
    --set train \
    --output_dir $OUTPUT_PATH \
    --arch $DINO_ARCH \
    --which_feature $LOST_FEATURES \
    --k_patches $K_PATCHES \
    --patch_size $PATCH_SIZE \
    --num_init_seeds $NUM_INIT_SEEDS

echo "COCO vit_small k=100 p=16, num_init_seed=100"
DINO_ARCH=vit_small
LOST_FEATURES=k
K_PATCHES=100
PATCH_SIZE=16
NUM_INIT_SEEDS=100

python main_lost.py \
    --dataset COCO20k \
    --set train \
    --output_dir $OUTPUT_PATH \
    --arch $DINO_ARCH \
    --which_feature $LOST_FEATURES \
    --k_patches $K_PATCHES \
    --patch_size $PATCH_SIZE \
    --num_init_seeds $NUM_INIT_SEEDS

echo "COCO vit_base k=50 p=16, num_init_seed=100"
DINO_ARCH=vit_base
LOST_FEATURES=k
K_PATCHES=50
PATCH_SIZE=16
NUM_INIT_SEEDS=100

python main_lost.py \
    --dataset COCO20k \
    --set train \
    --output_dir $OUTPUT_PATH \
    --arch $DINO_ARCH \
    --which_feature $LOST_FEATURES \
    --k_patches $K_PATCHES \
    --patch_size $PATCH_SIZE \
    --num_init_seeds $NUM_INIT_SEEDS

echo "COCO vit_base k=150 p=16, num_init_seed=100"
DINO_ARCH=vit_base
LOST_FEATURES=k
K_PATCHES=150
PATCH_SIZE=16
NUM_INIT_SEEDS=100

python main_lost.py \
    --dataset COCO20k \
    --set train \
    --output_dir $OUTPUT_PATH \
    --arch $DINO_ARCH \
    --which_feature $LOST_FEATURES \
    --k_patches $K_PATCHES \
    --patch_size $PATCH_SIZE \
    --num_init_seeds $NUM_INIT_SEEDS

echo "COCO vit_base k=100 p=8, num_init_seed=100"
DINO_ARCH=vit_base
LOST_FEATURES=k
K_PATCHES=100
PATCH_SIZE=8
NUM_INIT_SEEDS=100

python main_lost.py \
    --dataset COCO20k \
    --set train \
    --output_dir $OUTPUT_PATH \
    --arch $DINO_ARCH \
    --which_feature $LOST_FEATURES \
    --k_patches $K_PATCHES \
    --patch_size $PATCH_SIZE \
    --num_init_seeds $NUM_INIT_SEEDS

echo "COCO vit_base k=100 p=16, num_init_seed=25"
DINO_ARCH=vit_base
LOST_FEATURES=k
K_PATCHES=100
PATCH_SIZE=16
NUM_INIT_SEEDS=25

python main_lost.py \
    --dataset COCO20k \
    --set train \
    --output_dir $OUTPUT_PATH \
    --arch $DINO_ARCH \
    --which_feature $LOST_FEATURES \
    --k_patches $K_PATCHES \
    --patch_size $PATCH_SIZE \
    --num_init_seeds $NUM_INIT_SEEDS