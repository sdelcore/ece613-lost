#! /usr/bin/env bash

DATASET_ROOT_PATH=

# Build component versions
CMAKE_VERSION=3.16
CMAKE_BUILD=5
PYTHON_VERSION=3.7
TORCH_VERSION=1.8.1
TORCHVISION_VERSION=0.9.1

# Workspace structure in docker
LOST_ROOT=/root/lost
NUSC_ROOT=/root/nusc
CADC_ROOT=/root/cadc
COCO_ROOT=/root/coco
VOC_ROOT=/root/voc
LOGDIR=/root/logdir

# Workspace structure on host machine
HOST_LOST_ROOT=/home/sdelcore/wiselab/ece613-lost
HOST_NUSC_ROOT=/home/sdelcore/datasets/nuscenes
HOST_CADC_ROOT=/home/sdelcore/datasets/cadc
HOST_COCO_ROOT=/home/sdelcore/datasets/coco
HOST_VOC_ROOT=/home/sdelcore/datasets/voc
HOST_LOGDIR=/home/sdelcore/datasets/logdir