#! /usr/bin/env bash
source config.sh

docker run \
    --name="lost-ece613.$(whoami).$RANDOM" \
    --gpus=all \
    --rm \
    -it \
    -v "${HOST_LOST_ROOT}":"${LOST_ROOT}" \
    -v "${HOST_NUSC_ROOT}":"${NUSC_ROOT}" \
    -v "${HOST_CADC_ROOT}":"${CADC_ROOT}" \
    -v "${HOST_COCO_ROOT}":"${COCO_ROOT}" \
    -v "${HOST_VOC_ROOT}":"${VOC_ROOT}" \
    -v "${HOST_LOGDIR}":"${LOGDIR}" \
    -e DISPLAY=$DISPLAY \
    -e PUID=1000 \
    -e PGID=1000 \
    $@ \
    lost-ece613
    
    #    -p 5000:8888 \
#    -p 5001:6006 \
#    -p 5002:8889 \
#    -p 5003:8890 \
