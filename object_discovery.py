# Copyright 2021 - Valeo Comfort and Driving Assistance - Oriane Siméoni @ valeo.ai
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import enum
from matplotlib.pyplot import box
import torch
import scipy
import scipy.ndimage

import numpy as np
from datasets import _bbox_iou

def aspect_ratio(box):
    xdiff = abs(box[0] - box[2]) # Using absolute value to ignore negatives
    ydiff = abs(box[1] - box[3])
    return xdiff / ydiff

def box_area(box):
    xdiff = abs(box[0] - box[2]) # Using absolute value to ignore negatives
    ydiff = abs(box[1] - box[3])
    return xdiff * ydiff

def IOU_2D(box1, box2):
    ious, inter, union = _bbox_iou(box1, box2)
    for b in box2:
        print(box_area(b))
    print(box_area(box1), ious, inter, union)

    return ious

def lost(feats, dims, scales, init_image_size, k_patches=100, num_init_seeds=1, iou_threshold=0.1, num_sliding_windows=1):
    """
    Implementation of LOST method.
    Inputs
        feats: the pixel/patche features of an image
        dims: dimension of the map from which the features are used
        scales: from image to map scale
        init_image_size: size of the image
        k_patches: number of k patches retrieved that are compared to the seed at seed expansion
    Outputs
        pred: box predictions
        A: binary affinity matrix
        scores: lowest degree scores for all patches
        seed: selected patch corresponding to an object
    """
    # Compute the similarity
    A = (feats @ feats.transpose(1, 2)).squeeze()
    # Compute the inverse degree centrality measure per patch
    sorted_patches, scores = patch_scoring(A)

    if num_init_seeds <= 0:
        num_init_seeds = len(sorted_patches)
    
    num_init_seeds = min(num_init_seeds, len(sorted_patches))

    preds = []
    filtered_seeds= []

    # if only one seed, then do default LOST and return
    if num_init_seeds == 1:
        seed = sorted_patches[0]
        potentials = sorted_patches[:k_patches]
        similars = potentials[A[seed, potentials] > 0.0]
        M = torch.sum(A[similars, :], dim=0)
        pred, _ = detect_box(M, seed, dims, scales=scales, initial_im_size=init_image_size[1:])
        pred = np.asarray(pred)
        filtered_seeds.append(seed)
        preds.append(pred)
        return np.asarray(preds), A, scores, filtered_seeds
    
    #print('feats.shape',feats.shape)

    # Seed expansion
    for i in range(num_init_seeds):
        add_pred = False
        init_seed = sorted_patches[i]

        if scores[i] > 0:
            break
        #print('init_seed', init_seed)
        
        potentials = sorted_patches[i:k_patches+i]
        #print('potentials:',potentials)
        
        #  TODO: potentials should take into consideration distance from init seed
        #print("sim=A[seed, potentials]:", A[init_seed, potentials])
        similars = potentials[A[init_seed, potentials] > 0.0]
        #print("similars:",similars)
        #print("A[similars, :]",A[similars, :])

        M = torch.sum(A[similars, :], dim=0)

        # Box extraction
        pred, _ = detect_box(
            M, init_seed, dims, scales=scales, initial_im_size=init_image_size[1:]
        )
        pred = np.asarray(pred)


        #add_pred = aspect_ratio(pred) > 1.0 # TODO Remove assumption
        ious = 0
        
        ## TODO, if pick good iou
        ## if one bbox is completely inside another than pick the smaller one
        if len(preds) > 0:
            idx_to_remove = -1
            ious, inter, union = _bbox_iou(torch.from_numpy(pred), torch.from_numpy(np.asarray(preds)))
            add_pred = not any(ious >= iou_threshold)

        else:
            add_pred = True
            
        if add_pred:
            #print(ious, pred)
            filtered_seeds.append(init_seed)
            preds.append(pred)

        #raise Exception("ENDIT")  

    #print("Generated", len(preds), "predictions")
    return np.asarray(preds), A, scores, filtered_seeds


def patch_scoring(M, threshold=0.):
    """
    Patch scoring based on the inverse degree.
    """
    # Cloning important
    A = M.clone()

    # Zero diagonal
    A.fill_diagonal_(0)

    # Make sure symmetric and non nul
    A[A < 0] = 0
    C = A + A.t()

    # Sort pixels by inverse degree
    cent = -torch.sum(A > threshold, dim=1).type(torch.float32)
    sel = torch.argsort(cent, descending=True)

    return sel, cent


def detect_box(A, seed, dims, initial_im_size=None, scales=None):
    """
    Extract a box corresponding to the seed patch. Among connected components extract from the affinity matrix, select the one corresponding to the seed patch.
    """
    w_featmap, h_featmap = dims

    correl = A.reshape(w_featmap, h_featmap).float()

    # Compute connected components
    labeled_array, num_features = scipy.ndimage.label(correl.cpu().numpy() > 0.0)

    # Find connected component corresponding to the initial seed
    cc = labeled_array[np.unravel_index(seed.cpu().numpy(), (w_featmap, h_featmap))]

    # Should not happen with LOST
    if cc == 0:
        raise ValueError("The seed is in the background component.")

    # Find box
    mask = np.where(labeled_array == cc)
    # Add +1 because excluded max
    ymin, ymax = min(mask[0]), max(mask[0]) + 1
    xmin, xmax = min(mask[1]), max(mask[1]) + 1

    # Rescale to image size
    r_xmin, r_xmax = scales[1] * xmin, scales[1] * xmax
    r_ymin, r_ymax = scales[0] * ymin, scales[0] * ymax

    pred = [r_xmin, r_ymin, r_xmax, r_ymax]

    # Check not out of image size (used when padding)
    if initial_im_size:
        pred[2] = min(pred[2], initial_im_size[1])
        pred[3] = min(pred[3], initial_im_size[0])

    # Coordinate predictions for the feature space
    # Axis different then in image space
    pred_feats = [ymin, xmin, ymax, xmax]

    return pred, pred_feats


def dino_seg(attn, dims, patch_size, head=0):
    """
    Extraction of boxes based on the DINO segmentation method proposed in https://github.com/facebookresearch/dino. 
    """
    w_featmap, h_featmap = dims
    nh = attn.shape[1]
    official_th = 0.6

    # We keep only the output patch attention
    # Get the attentions corresponding to [CLS] token
    attentions = attn[0, :, 0, 1:].reshape(nh, -1)

    # we keep only a certain percentage of the mass
    val, idx = torch.sort(attentions)
    val /= torch.sum(val, dim=1, keepdim=True)
    cumval = torch.cumsum(val, dim=1)
    th_attn = cumval > (1 - official_th)
    idx2 = torch.argsort(idx)
    for h in range(nh):
        th_attn[h] = th_attn[h][idx2[h]]
    th_attn = th_attn.reshape(nh, w_featmap, h_featmap).float()

    # Connected components
    labeled_array, num_features = scipy.ndimage.label(th_attn[head].cpu().numpy())

    # Find the biggest component
    size_components = [np.sum(labeled_array == c) for c in range(np.max(labeled_array))]

    if len(size_components) > 1:
        # Select the biggest component avoiding component 0 corresponding to background
        biggest_component = np.argmax(size_components[1:]) + 1
    else:
        # Cases of a single component
        biggest_component = 0

    # Mask corresponding to connected component
    mask = np.where(labeled_array == biggest_component)

    # Add +1 because excluded max
    ymin, ymax = min(mask[0]), max(mask[0]) + 1
    xmin, xmax = min(mask[1]), max(mask[1]) + 1

    # Rescale to image
    r_xmin, r_xmax = xmin * patch_size, xmax * patch_size
    r_ymin, r_ymax = ymin * patch_size, ymax * patch_size
    pred = [r_xmin, r_ymin, r_xmax, r_ymax]

    return pred
